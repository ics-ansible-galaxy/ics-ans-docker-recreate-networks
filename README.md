# ics-ans-docker-recreate-networks

Ansible playbook to recreate all custom Docker networks on the host(s).

This playbook is not idempotent and should only be run on occasions when
the Docker networks needs to be recreated. The play will force the
presence of the networks, meaning recreating them from scratch and
reconnecting all containers again.

## License

BSD 2-clause
