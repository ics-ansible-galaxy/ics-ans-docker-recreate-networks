import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    with host.sudo():
        docker0_subnet = host.run("docker network inspect --format '{{(index .IPAM.Config 0).Subnet}}' bridge").stdout.strip()
        custom_subnet = host.run("docker network inspect --format '{{(index .IPAM.Config 0).Subnet}}' custom-network").stdout.strip()
    assert docker0_subnet == "172.21.0.0/24"
    assert custom_subnet == "172.21.1.0/24"
